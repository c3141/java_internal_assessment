package com.app.bank;

public class Ebank {
	
	private int accNumber;
	private String name;
	private String email;
	private String phoneNo;
	private int balance;
	
	
	public Ebank(int accNumber, String name, String email, String phoneNo, int balance) {
		super();
		this.accNumber = accNumber;
		this.name = name;
		this.email = email;
		this.phoneNo = phoneNo;
		this.balance = balance;
	}


	@Override
	public String toString() {
		return "Ebank [accNumber=" + accNumber + ", name=" + name + ", email=" + email + ", phoneNo=" + phoneNo
				+ ", balance=" + balance + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accNumber;
		result = prime * result + balance;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phoneNo == null) ? 0 : phoneNo.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ebank other = (Ebank) obj;
		if (accNumber != other.accNumber)
			return false;
		if (balance != other.balance)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phoneNo == null) {
			if (other.phoneNo != null)
				return false;
		} else if (!phoneNo.equals(other.phoneNo))
			return false;
		return true;
	}


	public int getAccNumber() {
		return accNumber;
	}


	public void setAccNumber(int accNumber) {
		this.accNumber = accNumber;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhoneNo() {
		return phoneNo;
	}


	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}


	public int getBalance() {
		return balance;
	}


	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	
	

}
