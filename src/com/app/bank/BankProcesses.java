package com.app.bank;

import java.util.ArrayList;
import java.util.List;

import custom_exceptions.AccountNotFoundException;
import custom_exceptions.InvalidStringException;

public interface BankProcesses {
	public abstract void addNewBankAccount(Ebank bankAccount,ArrayList<Ebank> accountList);
	public abstract void removeBankAccountByEmail(Ebank bankAccount,	ArrayList<Ebank> accountList) throws AccountNotFoundException;
	public abstract void updateBankAccount(Ebank bankAccount);
	public abstract String WithdrawFromBankAccount(int amount,int bankAccountNo,ArrayList<Ebank> accountList) throws AccountNotFoundException;
	public abstract String DepositeInBankAccount(int amount,int bankAccountNo,ArrayList<Ebank> accountList);
	public abstract String TransferFromBankAccount(int amount,int senderAccNumber,int recAccNo,ArrayList<Ebank> accountList);
	public abstract Ebank BankAccountByName(String name,ArrayList<Ebank> accountList)  throws InvalidStringException ;
	public abstract Ebank BankAccountByEmail(String email,ArrayList<Ebank> accountList) throws InvalidStringException;
	public abstract Ebank BankAccountByPhone(String phonenumber,ArrayList<Ebank> accountList) throws InvalidStringException;
	public abstract Ebank DisplayAccount(int bankAccountNo,ArrayList<Ebank> accountList);
	public abstract List<Ebank> showAllAccounts();

}
