package tester;


import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.Scanner;
import  static utils.CollectionUtils.populateSampleData;
import com.app.bank.Ebank;


import  utils.AccountUtils;





public class BankTester {
	public static void main(String... args) {

		try (Scanner sc = new Scanner(System.in)) {
			// init phase of the app
			// create suitable empty collection (AL) to store vehicle info in the showroom
			ArrayList<Ebank> accountList = populateSampleData();
			AccountUtils utils=new AccountUtils();
 
			boolean exit = false;
			while (!exit) {
				System.out.println("1.Add Account,2.Display Account,3.Display All,4.Remove Account,5.Withdraw,6.Deposit"
						+"7.Transfer,8.SearchByName,9.SearchByEmail,10.SearchByPhone,11.Exit");
				try {
					switch (sc.nextInt()) {
					case 1:

						System.out.println("Enter Account details : accNo,  AccountHolderName,  email,  "
								+ "pnoneNo,balance");
						//int accNumber, String name, String email, String phoneNo, int balance
						Ebank a1 = new Ebank (sc.nextInt(), sc.next(), sc.next(), sc.next(), sc.nextInt());
						
						accountList.add(a1);
						System.out.println("Account Added Successfully");

						break;

					case 2: // display
						System.out.println("Enter Account No to get details");
						a1=utils.DisplayAccount(sc.nextInt(),accountList);
						System.out.println(a1);
						break;
					case 3://display all 
						for (Ebank v : accountList)// for -each
							System.out.println(v);
						break;
					
						
					case 4:System.out.println("Add account details to  remove :accNumber, name,  email,  phoneNo, balance"); 
					    Ebank account=new Ebank(sc.nextInt(), sc.next(), sc.next(), sc.next(), sc.nextInt());
					    utils.removeBankAccountByEmail(account,accountList);
					    System.out.println("removed!!!");
						break;
					case 5:
						System.out.println("Enter amount and account no to withdraw");
						System.out.println(utils.WithdrawFromBankAccount(sc.nextInt(),sc.nextInt(),accountList));
						break;
					case 6:
						System.out.println("Enter amount and account no to Deposit");
						System.out.println(utils.DepositeInBankAccount(sc.nextInt(),sc.nextInt(),accountList));
						break;
					case 7:
						System.out.println("Enter amount To Transfer,senderAccountNo and recieversAccountNo ");
						System.out.println(utils.TransferFromBankAccount(sc.nextInt(),sc.nextInt(),sc.nextInt(),accountList));
						break;
					case 8:
						System.out.println("Enter Name to get AccountDetails ");
						System.out.println(utils.BankAccountByName(sc.next(),accountList));
						break;
					case 9:
						System.out.println("Enter Email to get AccountDetails ");
						System.out.println(utils.BankAccountByEmail(sc.next(),accountList));
						break;
					case 10:
						System.out.println("Enter PhoneNumber to get AccountDetails");
						System.out.println(utils.BankAccountByPhone(sc.next(),accountList));
						break;
			
					case 11:
						exit = true;
						break;
					}
				} // catch :
				catch (Exception e) {
					// e.printStackTrace();
					System.out.println(e.getMessage());
				}
				// to clear off pending tokens from the buffer of a scanner : till new line
				sc.nextLine();
			} // end of while
		} // end of try-with-res : sc.close();
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	}


