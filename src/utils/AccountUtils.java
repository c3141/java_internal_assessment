package utils;
import com.app.bank.*;

import static utils.CollectionUtils.populateSampleData;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.app.bank.BankProcesses;
import com.app.bank.Ebank;
import  static utils.CollectionUtils.populateSampleData;

import custom_exceptions.AccountNotFoundException;
import custom_exceptions.InvalidStringException;
import jdk.jfr.DataAmount;

public  class AccountUtils implements BankProcesses {
	// List<Ebank> accountList =new LinkedList<>();
	  
	ArrayList<Ebank>list=new ArrayList<Ebank>();
	@Override
	public void addNewBankAccount(Ebank bankAccount,ArrayList<Ebank> accountList) {
		// TODO Auto-generated method stub
		accountList.add(bankAccount);
	}

	@Override
	public  void removeBankAccountByEmail(Ebank bankAccount,ArrayList<Ebank> accountList) throws AccountNotFoundException {
		
		for(Ebank a:accountList) {
			System.out.println(a.getEmail());
			System.out.println(bankAccount.getEmail());
			if(a.getEmail().equals(bankAccount.getEmail())) {
				
				accountList.remove(a);
				
			}
		}
		throw new AccountNotFoundException();
	
	}

	@Override
	public void updateBankAccount(Ebank bankAccount) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String WithdrawFromBankAccount(int amount,int bankAccountNo,ArrayList<Ebank> accountList) throws AccountNotFoundException {
		
		for(Ebank a:accountList) {
			if(a.getAccNumber()==bankAccountNo) {
				if(a.getBalance()>100 && a.getBalance()>amount) {
					a.setBalance(a.getBalance()-amount);
					return "Balance deducted";
				}
				else {
					return "insufficient Balance";
				}
			}
		}
		throw new AccountNotFoundException(bankAccountNo);
		
	}

	@Override
	public String DepositeInBankAccount(int amount,int bankAccountNo,ArrayList<Ebank> accountList) {
		
		for(Ebank a:accountList) {
			if(a.getAccNumber()==bankAccountNo) {
				a.setBalance(a.getBalance()+amount);
				return "Balance Added";
			}
		}
		return "account not found";
		
		
		
	}

	@Override
	public String TransferFromBankAccount(int amount,int senderAccNumber,int recAccNo,ArrayList<Ebank> accountList) {
		for(Ebank a:accountList) {
			if(a.getAccNumber()==senderAccNumber) {
				for(Ebank b:accountList) {
					if(b.getAccNumber()==recAccNo) {
						if(a.getBalance()>amount)
						{
							b.setBalance(b.getBalance()+amount);
							a.setBalance(a.getBalance()-amount);
							return "Transaction Successfull";
						}
						else
						{
							return "insufficient balance";
						}
						
					}
				}
				return "recievers account not found!!!";
				
			}
		}
		return "account not found";
		
	}

	@Override
	public Ebank BankAccountByName(String name,ArrayList<Ebank> accountList) throws InvalidStringException {
		if(name.length()>=5) {
			for(Ebank a:accountList) {
				if(a.getName().equals(name)) {
					return a;
				}
			}
			return null;
		}
		throw new InvalidStringException(name);
	}

	@Override
	public Ebank BankAccountByEmail(String email,ArrayList<Ebank> accountList) throws InvalidStringException {
		if(email.contains("@")) {
			for(Ebank a:accountList) {
				if(a.getEmail().equals(email)) {
					return a;
				}
			}
			
		}
		throw new InvalidStringException(email);
	}

	@Override
	public Ebank BankAccountByPhone(String phonenumber,ArrayList<Ebank> accountList) throws InvalidStringException {
		if(phonenumber.length()==10) {
			
			for(Ebank a:accountList) {
				if(a.getPhoneNo().equals(phonenumber)) {
					return a;
				}
			}
		}
		
		throw new InvalidStringException(phonenumber);
	
	}

	@Override
	public List<Ebank> showAllAccounts() {
		// TODO Auto-generated method stub
		return list;
	}


	@Override
	public Ebank DisplayAccount(int bankAccountNo,ArrayList<Ebank> accountList) {
		for(Ebank a:accountList) {
			if(a.getAccNumber()==bankAccountNo) {
				return a;
			}
		
		
	}
		return null;

}
}
