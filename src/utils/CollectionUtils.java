package utils;



import java.text.ParseException;
import java.util.ArrayList;

import com.app.bank.Ebank;

import custom_exceptions.AccountNotFoundException;

//int accNumber, String name, String email, String phoneNo, int balance

public class CollectionUtils {

	
		//add a static method to return populated list of vehicles
			public static ArrayList<Ebank> populateSampleData() throws ParseException, AccountNotFoundException {
				ArrayList<Ebank> list = new ArrayList<>();
				// chasisNo, Color color, double price, Date manufactureDate, Category category
				list.add(new Ebank(123, "MAYUR", "abc@gmail.com", "7028512954", 10000));
				list.add(new Ebank(124, "Hrushi", "xyz@gmail.com", "1234567890", 100000));
				list.add(new Ebank(125, "Pk", "Pk@gmail.com", "8483512954", 12000));
				list.add(new Ebank(126, "Usman", "chu@gmail.com", "8028512954", 101));
			
				return list;
			}
		}

