package custom_exceptions;

public class AccountNotFoundException extends Exception {
	private int accNumber;

	public AccountNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AccountNotFoundException(int accNumber) {
		super();
		this.accNumber = accNumber;
	}

	@Override
	public String toString() {
		return "AccountNotFoundException [accNumber=" + accNumber + "]";
	}
	

}
